import unittest

from debian_image_finder.tests.base import BeforeTestUserCommon
from debian_image_finder.models import User
from debian_image_finder import db
from debian_image_finder import app
import uuid
import datetime
import jwt
import json
from werkzeug.security import generate_password_hash


def generateValidToken():
    hashed_password = generate_password_hash('123456', method='sha256')

    user = User(public_id=str(uuid.uuid4()),
                name='user',
                password=hashed_password,
                admin=True)

    db.session.add(user)
    db.session.commit()

    exp = datetime.datetime.utcnow() + datetime.timedelta(minutes=30)
    token = jwt.encode({'public_id': user.public_id, 'exp': exp},
                       app.config['SECRET_KEY'])
    return token


class TestUserService(BeforeTestUserCommon):
    """Tests for the User Service."""

    def test_create_user(self):

        token = generateValidToken()

        args = {
            "name": "test",
            "password": "123456"
        }
        headers = {
            'Content-Type': 'application/json',
            'x-access-token': token
        }

        response = self.client.post("/api/user",
                                    data=json.dumps(args),
                                    headers=headers)
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('New user created!', data['message'])

    def test_get_user(self):
        """Ensure the /user route behaves correctly."""

        token = generateValidToken()

        hashed_password = generate_password_hash('123456', method='sha256')
        user = User(public_id=str(uuid.uuid4()),
                    name='user',
                    password=hashed_password,
                    admin=False)
        db.session.add(user)
        db.session.commit()

        headers = {
            'Content-Type': 'application/json',
            'x-access-token': token
        }

        response = self.client.get('/api/user/' + user.public_id,
                                   headers=headers)
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('user', data['name'])
        self.assertEqual(False, data['admin'])
        self.assertEqual(user.public_id, data['public_id'])
        self.assertEqual(2, data['id'])

    def test_promote_user(self):
        """Ensure the promote user route behaves correctly."""

        token = generateValidToken()

        hashed_password = generate_password_hash('123456', method='sha256')
        user = User(public_id=str(uuid.uuid4()),
                    name='user',
                    password=hashed_password,
                    admin=False)
        db.session.add(user)
        db.session.commit()

        headers = {
            'Content-Type': 'application/json',
            'x-access-token': token
        }

        response = self.client.put('/api/user/promote/' + user.public_id,
                                   headers=headers)
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('The user has been promoted!', data['message'])

    def test_unpromote_user(self):
        """Ensure the unpromote user route behaves correctly."""

        token = generateValidToken()

        hashed_password = generate_password_hash('123456', method='sha256')
        user = User(public_id=str(uuid.uuid4()),
                    name='user',
                    password=hashed_password,
                    admin=False)
        db.session.add(user)
        db.session.commit()

        headers = {
            'Content-Type': 'application/json',
            'x-access-token': token
        }

        response = self.client.put('/api/user/unpromote/' + user.public_id,
                                   headers=headers)
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('The user has been unpromoted!', data['message'])

    def test_delete_user(self):
        """Ensure the delete user route behaves correctly."""

        token = generateValidToken()

        hashed_password = generate_password_hash('123456', method='sha256')
        user = User(public_id=str(uuid.uuid4()),
                    name='user',
                    password=hashed_password,
                    admin=False)
        db.session.add(user)
        db.session.commit()

        headers = {
            'Content-Type': 'application/json',
            'x-access-token': token
        }

        response = self.client.delete('/api/user/' + user.public_id,
                                      headers=headers)
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('The user has been deleted!', data['message'])

    def test_login_user(self):
        """Ensure the login user route behaves correctly."""

        hashed_password = generate_password_hash('123456', method='sha256')
        user = User(public_id=str(uuid.uuid4()),
                    name='user',
                    password=hashed_password,
                    admin=False)
        db.session.add(user)
        db.session.commit()

        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Basic dXNlcjoxMjM0NTY='
        }

        response = self.client.get('/api/login',
                                   headers=headers)
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()
