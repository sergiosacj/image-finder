import json
import unittest

from debian_image_finder.tests.base import BeforeTestUserCommon
from debian_image_finder.models import Provider, Image
from debian_image_finder import db


class TestImageService(BeforeTestUserCommon):
    """Tests for the Image Service."""

    def test_post_image(self):

        provider = Provider(name='provider',
                            description='test',
                            markdown_file='file')
        db.session.add(provider)
        db.session.commit()

        args = {
            "arch": "arch",
            "release": "release",
            "img_type": "img_type",
            "vendor": "vendor",
            "version": "version",
            "region": "region",
            "ref": "/ref.tar.xz",
            "packages": "packages",
            "provider_id": 1
        }
        headers = {
            'Content-Type': 'application/json',
            'x-access-token': self.token
        }

        response = self.client.post("/api/image",
                                    data=json.dumps(args),
                                    headers=headers)
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('arch', data['arch'])
        self.assertIn('release', data['release'])
        self.assertIn('img_type', data['img_type'])
        self.assertIn('vendor', data['vendor'])
        self.assertIn('version', data['version'])
        self.assertIn('region', data['region'])
        self.assertIn('ref', data['ref'])
        self.assertIn('packages', data['packages'])
        self.assertEqual(1, data['provider_id'])

    def test_get_image(self):
        """Ensure the /image route behaves correctly."""

        provider = Provider(name='provider',
                            description='test',
                            markdown_file='file')
        db.session.add(provider)
        db.session.commit()

        image = Image(arch='arch',
                      release='release',
                      img_type='img_type',
                      vendor='vendor',
                      version='version',
                      region='region',
                      ref='/ref.tar.xz',
                      packages='packages',
                      created_at='2017-08-09T00:00:00',
                      provider_id=1)

        db.session.add(image)
        db.session.commit()

        response = self.client.get('/api/image/1')
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('arch', data['arch'])
        self.assertIn('release', data['release'])
        self.assertIn('img_type', data['img_type'])
        self.assertIn('vendor', data['vendor'])
        self.assertIn('version', data['version'])
        self.assertIn('region', data['region'])
        self.assertIn('ref', data['ref'])
        self.assertIn('packages', data['packages'])
        self.assertIn('2017-08-09T00:00:00', data['created_at'])
        self.assertEqual(1, data['provider_id'])

    def test_put_image(self):
        """Ensure the /image route behaves correctly."""

        provider = Provider(name='provider',
                            description='test',
                            markdown_file='file')
        db.session.add(provider)
        db.session.commit()

        image = Image(arch='arch',
                      release='release',
                      img_type='img_type',
                      vendor='vendor',
                      version='version',
                      region='region',
                      ref='/ref.tar.xz',
                      packages='packages',
                      created_at='2018-08-09',
                      provider_id=1)

        db.session.add(image)
        db.session.commit()
        args = {
            "arch": "arch_change",
            "release": "release_change",
            "img_type": "img_type_change",
            "vendor": "vendor_change",
            "version": "version_change",
            "region": "region_change",
            "ref": "/ref_change.tar.xz",
            "packages": "packages_change",
            "provider_id": 1
        }

        headers = {
            'Content-Type': 'application/json',
            'x-access-token': self.token
        }

        response = self.client.put("/api/image/1",
                                   data=json.dumps(args),
                                   headers=headers)
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('arch_change', data['arch'])
        self.assertIn('release_change', data['release'])
        self.assertIn('img_type_change', data['img_type'])
        self.assertIn('vendor_change', data['vendor'])
        self.assertIn('version_change', data['version'])
        self.assertIn('region_change', data['region'])
        self.assertIn('ref_change', data['ref'])
        self.assertIn('packages_change', data['packages'])
        self.assertIn('2018-08-09T00:00:00', data['created_at'])
        self.assertEqual(1, data['provider_id'])

    def test_delete_image(self):
        """Ensure the /image route behaves correctly."""

        provider = Provider(name='provider',
                            description='test',
                            markdown_file='file')
        db.session.add(provider)
        db.session.commit()

        image = Image(arch='arch',
                      release='release',
                      img_type='img_type',
                      vendor='vendor',
                      version='version',
                      region='region',
                      ref='/ref.tar.xz',
                      packages='packages',
                      created_at='2017-08-09',
                      provider_id=1)

        db.session.add(image)
        db.session.commit()

        headers = {
            'Content-Type': 'application/json',
            'x-access-token': self.token
        }

        response = self.client.delete('/api/image/1',
                                      headers=headers)
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('arch', data['arch'])
        self.assertIn('release', data['release'])
        self.assertIn('img_type', data['img_type'])
        self.assertIn('vendor', data['vendor'])
        self.assertIn('version', data['version'])
        self.assertIn('region', data['region'])
        self.assertIn('ref', data['ref'])
        self.assertIn('packages', data['packages'])
        self.assertIn('2017-08-09T00:00:00', data['created_at'])
        self.assertEqual(1, data['provider_id'])


if __name__ == '__main__':
    unittest.main()
